
const express = require('express');
const session = require('express-session')
const mongoose = require('mongoose');
const multer = require('multer');
const MongoStore = require('connect-mongo');
const upload = multer();
const uuid = require('uuid');
const bodyParser = require('body-parser');
const User = require('./models/userModel');
const Product = require('./models/productModel');
const order = require('./models/orderModel');
const favorite = require('./models/favoriteModel');
const cors = require('cors');


const app = express();
const port = process.env.PORT || 3001;

const CONNECTION_URL = "mongodb+srv://admin:admin@cluster0.982co.mongodb.net/cloth?retryWrites=true";
const DATABASE_NAME = "cloth";

mongoose.Promise = global.Promise;
mongoose.connect(CONNECTION_URL, { useNewUrlParser: true ,useUnifiedTopology: true});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.set('trust proxy', 1);
app.use(session({
    secret: 'secret',
    store : MongoStore.create({ mongoUrl:'mongodb+srv://admin:admin@cluster0.982co.mongodb.net/sessiondb' }),
    resave: false,
    saveUninitialized: true,
    cookie : { maxAge: 60*60*24*1000 , secure: true , HttpOnly: true, Path:"/"  , sameSite : 'none'} // 1 day
  }));
  
app.use(upload.array()); 
app.use(cors({ origin : 'https://agileproject100.gitlab.io' , credentials: true}));
app.use('/colthImage',express.static('colthImage'));



var routes = require('./routes/routes.js');
routes(app);

app.listen(port);

console.log('RESTful API server started on: ' + port);