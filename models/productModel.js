
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema;


var ProductSchema = new Schema({
  productName : {
    type: String
  },
  content : {
    type: String
  },
  price : {
    type: Number
  },
  image : {
    type: String
  }
});

module.exports = mongoose.model('Product', ProductSchema);