



module.exports = function(app) {
    var register = require('../controllers/registerController');
    var user = require('../controllers/userController');
    var product = require('../controllers/productController');
    var order = require('../controllers/orderController');
    var favorite = require('../controllers/favoriteController');
    var profile = require('../controllers/profileController');

    // Routes
    app.route('/register').post(register.register_account)

    app.route('/login').post(user.login)
    app.route('/loginStatus').get(user.login_status)
    app.route('/logout').get(user.logout)

    app.route('/profile').put(profile.updatePw)

    app.route('/product').get(product.getAllProduct)
    app.route('/product/:productName').get(product.getProductByName)
    app.route('/productById/:productId').get(product.getProductById)

    app.route('/order').get(order.getOrderById).delete(order.deleteAllOrder)
    app.route('/createOrder').post(order.addOrder)
    app.route('/order/:orderId').put(order.updateOrderById).delete(order.deleteOrderById)

    app.route('/favorite').get(favorite.getFavoriteByUid)
    app.route('/favorite/:productId').get(favorite.getFavoriteByPid)
    app.route('/createfavorite').post(favorite.addFavorite)
    app.route('/favorite/:productId').delete(favorite.deleteFavoriteById)
    

};