
const mongoose = require('mongoose');
const Product = mongoose.model('Product');


exports.getAllProduct = function(req, res) {

    Product.find({},function(err, product) {
        if (err){
            res.status(400).send({
                products: null,
                status : 'F'
            });
        }else{
            res.json({
                products: product,
                status : 'S'
            });
        }
    });

};

exports.getProductByName = function(req, res) {

    var productName = req.params.productName
    

    Product.find({ 'productName': new RegExp(productName, 'i') },function(err, product) {
        if (err){
            res.status(400).send({
                products: null,
                status : 'F'
            });
        }else{
            res.json({
                products : product,
                status : 'S',
            });
        }
    });
    
};

exports.getProductById = function(req, res) {

    var productId = req.params.productId
    

    Product.findOne({ '_id': productId },function(err, product) {
        if (err){
            res.status(400).send({
                products: null,
                status : 'F'
            });
        }else{
            res.json({
                product : product,
                status : 'S',
            });
        }
    });
    
};

