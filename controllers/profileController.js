
const mongoose = require('mongoose');
const User = mongoose.model('User');

exports.updatePw = function(req, res) {

    if(req.session.user === null || req.session.user === undefined){
        return res.json({
            status : 'U',
            message : "Please Login first."
        })
    }else{

        var uid = req.session.user.uid;
        var oldpw = req.body.oldPassword;
        var newPassword = req.body.newPassword;
        var userdetail = {};

        User.findOne({_id : uid , password : oldpw }, function(err, user) {

            if (err){
                return res.send(400, err)
            }else if(user === {} || user === undefined || user === null  ){
                return res.json({
                    status : 'N',
                    message : "The old password is not correct."
                });
            }else{
                console.log(user) ;
                userdetail = {
                    userName : user.userName,
                    password : newPassword,
                };


                User.findOneAndUpdate(uid, userdetail, { new: true }, function(err,user) {
                    if (err){
                        return res.status(400).send({
                            status : 'F',
                            message : "Update password failed."
                        })
                    }else{
                        return res.json({
                            status : 'S',
                            message : "The password is Updated."
                        });
                    }
                    
                    
                });

            }
        })

       
    }

};