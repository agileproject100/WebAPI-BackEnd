
const mongoose = require('mongoose');
const Order = mongoose.model('Order');

exports.getOrderById = function(req, res) {

    if(req.session.user === null || req.session.user === undefined){
        return res.json({
            orders : null,
            status : 'U',
            message : "Please Login first."
        })
    }else{

        var userId = req.session.user.uid

        Order.find({ userId : userId },function(err, order) {
            if (err){
                res.status(400).send({
                    orders : null,
                    status : 'F',
                    message : "Get order failed."
                });
            }else{
                return res.json({
                    orders : order,
                    status : 'S',
                    message : "Order is getted."
                })
            }
        });
    }

};

exports.addOrder = function(req, res) {

    if(req.session.user === null || req.session.user === undefined){
        return res.json({
            status : 'U',
            message : "Please Login first."
        })
    }else{

        var product = {
            userId : req.session.user.uid,
            productName : req.body.productName,
            price : req.body.price,
            quantity : req.body.quantity,
            image : req.body.image,
        }

        var product_s = new Order(product)


        product_s.save(function(err, order) {
            if (err){
                res.status(400).send({
                    message: "Place order failed.",
                    status : 'F'
                });
            }else{
                return res.json({
                    status : 'S',
                    message : "The order is added."
                });
            }
        });
    }

};

exports.updateOrderById = function(req, res) {

    if(req.session.user === null || req.session.user === undefined){
        return res.json({
            status : 'U',
            message : "Please Login first."
        })
    }else{

        var orderId = req.body._id
        var order = {
            userId : req.body.userId,
            productName : req.body.productName,
            price : req.body.price,
            quantity : req.body.quantity,
            image : req.body.image,
        }
       

        Order.findOneAndUpdate(orderId, order, { new: true }, function(err, order) {
            if (err){
                res.status(400).send({
                    status : 'F',
                    message : "Update order failed."
                });
            }else{
                return res.json({
                    status : 'S',
                    message : "The Order is Updated."
                });
            }
            
            
        });
    }

};

exports.deleteOrderById = function(req, res) {

    if(req.session.user === null || req.session.user === undefined){
        return res.json({
            status : 'U',
            message : "Please Login first."
        })
    }else{

        var orderId = req.params.orderId

        Order.remove({
            _id: orderId
        }, function(err, order) {
            if (err){
                res.status(400).send({
                    status : 'F',
                    message : "Delete order failed."
                });
            }else{
                return res.json({
                    status : 'S',
                    message : "Order successfully deleted."
                })
            }
                
        });

    }

};

exports.deleteAllOrder = function(req, res) {

    if(req.session.user === null || req.session.user === undefined){
        return res.json({
            status : 'U',
            message : "Please Login first."
        })
    }else{

        var userId = req.session.user.uid

        Order.remove({
            userId: userId
        }, function(err, order) {
            if (err){
                return res.send(err);
            }else{
                return res.json({
                    status : 'S',
                    message : "The order is purchased."
                })
            }
                
        });

    }

};