

const mongoose = require('mongoose');
const uuid = require('uuid');
const User = mongoose.model('User');


exports.login = function(req, res) {

    var current_user = new User(req.body);

    User.findOne({userName : current_user.userName , password : current_user.password }, function(err, user) {
        
        if(user === null){
            res.json({
                message : 'user name or password is not correct.',
                status : 'W'
            });
        }else if (err){
            res.status(401).send({
                message : 'Login failed.',
                status : 'F'
            });
        }else{
            var token_res = {
                uid : user._id,
                userName : user.userName,
            };

            req.session.user = token_res;

            console.log(req.session.user);


            res.json({
                user : {...req.session.user},
                message : 'Login successful.',
                status : 'S'
            });
        }
        
    });

};

exports.login_status = function(req, res) {

    // const body_req = req.body;
    // const current_token = body_req.token;
    // const session_token = req.session.user.token;
    console.log(req.session.user);
    if(req.session.user === null || req.session.user === undefined){
        res.status(401).send({message : "Session Expired."})
    }else{
        res.json(req.session.user)
    }
    
};


exports.logout = function(req, res) {

    req.session.destroy();
    
    res.json({message : "Logout."});

};