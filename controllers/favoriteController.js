const mongoose = require('mongoose');
const Favorite = mongoose.model('Favorite');

exports.getFavoriteByUid = function(req, res) {

    if(req.session.user === null || req.session.user === undefined){
        return res.json({
            favorite: null,
            status : 'U',
            message : "Please Login first."
        })
    }else{

        var userId = req.session.user.uid

        Favorite.find({ userId : userId },function(err, favorite) {
            if (err){
                res.status(400).send({
                    favorite: null,
                    status : 'F',
                    message : "Get favorite failed."
                });
            }else{
                return res.json({
                    favorite : favorite,
                    status : 'S',
                    message : "Loading Successful."
                });
            }
        });
    }

};

exports.addFavorite = function(req, res) {

    if(req.session.user === null || req.session.user === undefined){
        return res.json({
            status : 'U',
            message : "Please Login first."
        })
    }else{

        var favorite = {
            userId : req.session.user.uid,
            productId : req.body.productId,
            productName : req.body.productName,
            price : req.body.price,
            image : req.body.image,
        }

        var favorite_s = new Favorite(favorite)


        favorite_s.save(function(err, favorite) {
            if (err){
                res.status(400).send({
                    favorite: null,
                    status : 'F',
                    message : "Add favorite failed."
                });
            }else{
                return res.json({
                    status : 'S',
                    message : 'Favorite is added.'
                });
            }
        });
    }

};

exports.getFavoriteByPid = function(req, res) {

    if(req.session.user === null || req.session.user === undefined){
        return res.json({
            status : 'U',
            message : "The user have not been login."
        })
    }else{

        var userId = req.session.user.uid
        var productId = req.params.productId

        Favorite.findOne({ userId : userId , productId : productId },function(err, favorite) {
            if (err){
                res.status(400).send({
                    status : 'F',
                    message : "Get Favorite failed."
                });
            }else if(favorite !== {} && favorite !== null && favorite !== undefined){
                return res.json({
                    status : 'S',
                    message : "Favorite is exist."
                });
            }else{
                return res.json({
                    status : 'N',
                    message : "Favorite is not exist."
                });
            }
        });
    }

};

exports.deleteFavoriteById = function(req, res) {

    if(req.session.user === null || req.session.user === undefined){
        return res.json({
            status : 'U',
            message : "Please Login first."
        })
    }

    var productId = req.params.productId

    Favorite.remove({
        productId: productId,
        userId : req.session.user.uid
    }, function(err, favorite) {
        if (err){
            return res.status(400).send({
                status : 'F',
                message : "Delete favorite failed."
            });
        }
        return res.json({ status : 'S', message: 'Favorite is deleted.' });
    });

};