
var mongoose = require('mongoose'); 
var Schema = mongoose.Schema;


var OrderSchema = new Schema({
  userId : {
    type: String
  },
  productName : {
    type: String
  },
  price : {
    type: Number
  },
  quantity : {
    type: Number
  },
  image : {
    type: String
  }
});

module.exports = mongoose.model('Order', OrderSchema);