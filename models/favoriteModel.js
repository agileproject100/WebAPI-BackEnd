var mongoose = require('mongoose'); 
var Schema = mongoose.Schema;


var FavoriteSchema = new Schema({
  userId : {
    type: String
  },
  productId : {
    type: String
  },
  productName : {
    type: String
  },
  content : {
    type: String
  },
  price : {
    type: String
  },
  image : {
    type: String
  }
});

module.exports = mongoose.model('Favorite', FavoriteSchema);